# Handlebars Spec

[![Build Status](https://travis-ci.org/jbboehr/handlebars-spec.svg?branch=master)](https://travis-ci.org/jbboehr/handlebars-spec)

The [handlebars.js](https://github.com/wycats/handlebars.js) specification converted to JSON.

This project was [originally developed](https://github.com/kasperisager/handlebars-spec) by [Kasper Isager](https://github.com/kasperisager).


## License

This project is licensed under the [LGPLv3](http://www.gnu.org/licenses/lgpl-3.0.txt). The specification data is part
of [handlebars.js](https://github.com/wycats/handlebars.js) and is licensed under the [MIT license](http://opensource.org/licenses/MIT).
